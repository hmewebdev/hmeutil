﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using HiQPdf;
using System.IO;

namespace hmeUtil
{
    public class ConvertHtmlToPdf
    {
        public void convert()
        {
            HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
            htmlToPdfConverter.SerialNumber = "mtLzy8r+-/Nbz+Oj7-6OOrqrSq-uqu6qLqs-o6y6qau0-q6i0o6Oj-ow==";

            // convert URL to a PDF buffer in memory
            // the buffer can be saved to a file, a stream or a HTTP response
            byte[] pdfBuffer = htmlToPdfConverter.ConvertUrlToMemory("http://www.google.com");

            // convert URL to a PDF file
            htmlToPdfConverter.Document.PageOrientation = PdfPageOrientation.Landscape;
            htmlToPdfConverter.ConvertUrlToFile("http://www.google.com", @"c:\dev\result.pdf");
            htmlToPdfConverter = null;
        }

        public byte[] convertURLToByteArray(string url)
        {
            HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
            htmlToPdfConverter.SerialNumber = "mtLzy8r+-/Nbz+Oj7-6OOrqrSq-uqu6qLqs-o6y6qau0-q6i0o6Oj-ow==";
            byte[] pdfBuffer = htmlToPdfConverter.ConvertUrlToMemory(url);
            return pdfBuffer;
        }

        public Stream convertURLToStream(string url)
        {
            HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
            htmlToPdfConverter.SerialNumber = "mtLzy8r+-/Nbz+Oj7-6OOrqrSq-uqu6qLqs-o6y6qau0-q6i0o6Oj-ow==";
            Stream pdfStream = null;
            htmlToPdfConverter.ConvertHtmlToStream(url, null, pdfStream);
            return pdfStream;
        }

        public void convertURLToFile(string url, string filepath, string baseURL, string orientation, int waitTime)
        {
            HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
            htmlToPdfConverter.SerialNumber = "mtLzy8r+-/Nbz+Oj7-6OOrqrSq-uqu6qLqs-o6y6qau0-q6i0o6Oj-ow==";
            switch (orientation)
            {
                case "Landscape":
                    htmlToPdfConverter.Document.PageOrientation = PdfPageOrientation.Landscape;
                    break;
                case "Portrait":
                    htmlToPdfConverter.Document.PageOrientation = PdfPageOrientation.Portrait;
                    break;
                default:
                    htmlToPdfConverter.Document.PageOrientation = PdfPageOrientation.Portrait;
                    break;
            }
            htmlToPdfConverter.WaitBeforeConvert = waitTime;
            htmlToPdfConverter.ConvertUrlToFile(url, filepath);
        }

        public byte[] convertHtmlToByteArray(string html)
        {
            HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
            htmlToPdfConverter.SerialNumber = "mtLzy8r+-/Nbz+Oj7-6OOrqrSq-uqu6qLqs-o6y6qau0-q6i0o6Oj-ow==";
            byte[] pdfBuffer = htmlToPdfConverter.ConvertHtmlToMemory(html,null);
            htmlToPdfConverter = null;
            return pdfBuffer;
        }

        public void convertHtmlToFile(string html, string filepath, string baseURL, string orientation)
        {
            HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
            htmlToPdfConverter.SerialNumber = "mtLzy8r+-/Nbz+Oj7-6OOrqrSq-uqu6qLqs-o6y6qau0-q6i0o6Oj-ow==";
            switch (orientation)
            {
                case "Landscape":
                    htmlToPdfConverter.Document.PageOrientation = PdfPageOrientation.Landscape;
                    break;
                case "Portrait":
                    htmlToPdfConverter.Document.PageOrientation = PdfPageOrientation.Portrait;
                    break;
                default:
                    htmlToPdfConverter.Document.PageOrientation = PdfPageOrientation.Portrait;
                    break;
            }
            htmlToPdfConverter.Document.FitPageHeight = true;
            htmlToPdfConverter.Document.FitPageWidth = true;
            htmlToPdfConverter.Document.ForceFitPageWidth = true;
            htmlToPdfConverter.WaitBeforeConvert = 15;
            htmlToPdfConverter.ConvertHtmlToFile(html, baseURL, filepath);
        }

        public Stream convertHtmlToStream(string html)
        {
            HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
            htmlToPdfConverter.SerialNumber = "mtLzy8r+-/Nbz+Oj7-6OOrqrSq-uqu6qLqs-o6y6qau0-q6i0o6Oj-ow==";
            Stream pdfStream = null;
            htmlToPdfConverter.ConvertHtmlToStream(html, null, pdfStream);
            return pdfStream;
        }

    }
}
