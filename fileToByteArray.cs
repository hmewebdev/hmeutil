﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hmeUtil
{
    public class FileIO
    {
        public static byte[] FileToByteArray(string filePath)
        {
            byte[] buff = null;
            using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                using (BinaryReader br = new BinaryReader(fs))
                {
                    long numBytes = new FileInfo(filePath).Length;
                    buff = br.ReadBytes((int)numBytes);
                }
            }
            return buff;
        }

        public static MemoryStream FileToMemoryStream(string filePath)
        {
            MemoryStream memStream = null;
            using (FileStream fileStream = File.OpenRead(filePath))
            {
                memStream = new MemoryStream();
                memStream.SetLength(fileStream.Length);
                fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
            }
            return memStream;
        }

    }
}
