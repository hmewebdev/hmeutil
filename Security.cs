﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;


namespace hmeUtil
{
    public class Security
    {
        public static string salt = "@0d4k?@8j*f%1l93sdf903jh^^oower&";

        #region validateToken
        #region UserToken
        public userinfo_dto UserToken(string[] userInfo, string ip, string pc)
        {
            Encryptor encryptor = new Encryptor();
            JavaScriptSerializer json = new JavaScriptSerializer();
            userinfo_dto userinfo_dto = new userinfo_dto();
            userinfo_dto.userinfo = new userinfo();
            userinfo_dto.userinfo.email = userInfo[0];
            userinfo_dto.userinfo.fullname = userInfo[1];
            userinfo_dto.userinfo.userid = userInfo[2];
            userinfo_dto.userinfo.firstname = userInfo[3];
            userinfo_dto.userinfo.lastname = userInfo[4];
            userinfo_dto.userinfo.roles = userInfo[5];
            userinfo_dto.userinfo.id = userInfo[6];
            userinfo_dto.userinfo.company = userInfo[7];
            userinfo_dto.userinfo.department = userInfo[8];
            userinfo_dto.userinfo.location = userInfo[9];
            userinfo_dto.token = encryptor.Encrypt(json.Serialize(userinfo_dto.userinfo), salt);
            //userinfo_dto.ts = encryptor.Encrypt(json.Serialize(TimeStamp(userInfo[6], userInfo[2], ip, pc)), salt);
            //userinfo_dto.ip = encryptor.Encrypt(ip, model.salt);
            userinfo_dto.ip = ip;
            userinfo_dto.name = pc;
            return userinfo_dto;
        }
        #endregion
        #endregion

        #region validateToken
        #region TimeStamp
        public object TimeStamp(string clientid, string userid, string ip, string pc)
        {
            DateTime date = DateTime.Now;
            long ticks = date.Ticks;
            ts_dto ts_dto = new ts_dto();
            ts_dto.ts = ticks.ToString();
            ts_dto.clientid = clientid;
            ts_dto.userid = userid;
            ts_dto.name = pc;
            ts_dto.ip = ip;
            return ts_dto;
        }
        #endregion
        #endregion

        #region validateToken
        public bool ValidateToken(string token, string clientid, string userid, string ip, string pc, int elapsed, string timeFrame)
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            bool status = true;
            bool did_elapse = false;
            DateTime date = DateTime.Now;
            Encryptor encryptor = new Encryptor();
            ts_dto ts_dto = json.Deserialize<ts_dto>(encryptor.Decrypt(token, salt));
            long ticks = date.Ticks;
            try
            {
                long token_ticks = (long)Convert.ToDouble(ts_dto.ts);
                long elapsedTicks = ticks - token_ticks;
                TimeSpan elapsedSpan = new TimeSpan(elapsedTicks);
                double elapsedSeconds = elapsedSpan.TotalSeconds;
                double elapsedMinutes = elapsedSpan.Minutes;
                double elapsedHours = elapsedSpan.Hours;
                double elapsedDays = elapsedSpan.Days;
                double elapsedMS = elapsedSpan.Milliseconds;
                if (elapsed != 0)
                {
                    switch (timeFrame)
                    {
                        case "milliseconds":
                            did_elapse = elapsedDays > elapsed;
                            break;
                        case "seconds":
                            did_elapse = elapsedSeconds > elapsed;
                            //if (elapsedSeconds > elapsed)
                            //{
                            //    return false;
                            //}
                            break;
                        case "minutes":
                            did_elapse = elapsedMinutes > elapsed;
                            break;
                        case "hours":
                            did_elapse = elapsedHours > elapsed;
                            break;
                        case "days":
                            did_elapse = elapsedDays > elapsed;
                            break;
                    }

                    if (did_elapse) return !did_elapse;
                }

                if (ip != null && ts_dto.ip != ip)
                {
                    status = false;
                }
                if (pc != null && ts_dto.name != pc)
                {
                    status = false;
                }
                if (clientid != null && ts_dto.clientid != clientid)
                {
                    status = false;
                }
                if (userid != null && ts_dto.userid != userid)
                {
                    status = false;
                }

            }
            catch (Exception ex) { var msg = ex.Message; status = false; }
            return status;
        }
        #endregion

        #region Objects

        [Serializable]
        public class userinfo_dto
        {
            public userinfo userinfo { get; set; }
            public string token { get; set; }
            public string ip { get; set; }
            public string name { get; set; }
            public string ts { get; set; }
            public string srclientid { get; set; }
        }

        [Serializable]
        public class userinfo
        {
            public string email { get; set; }
            public string fullname { get; set; }
            public string userid { get; set; }
            public string firstname { get; set; }
            public string lastname { get; set; }
            public string roles { get; set; }
            public string id { get; set; }
            public string company { get; set; }
            public string department { get; set; }
            public string location { get; set; }
        }

        [Serializable]
        public class ts_dto
        {
            public string ts { get; set; }
            public string clientid { get; set; }
            public string userid { get; set; }
            public string ip { get; set; }
            public string name { get; set; }
            public string app { get; set; }
        }

        [Serializable]
        public class Token
        {
            public TokenObj token { get; set; }
            public ts_dto ts { get; set; }
            public bool valid { get; set; }
        }

        [Serializable]
        public class TokenObj
        {
            public string client { get; set; }
            public string requester { get; set; }
            public string id { get; set; }
            public string ip { get; set; }
        }

        #endregion
    }
}
