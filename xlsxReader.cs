﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel;
using System.Data;
using System.IO;
using OfficeOpenXml;

namespace hmeUtil
{
    public class xlsxReader
    {
        public MemoryStream TableToExcel(DataTable tbl, string sheet)
        {
            using (ExcelPackage pack = new ExcelPackage())
            {
                ExcelWorksheet ws = pack.Workbook.Worksheets.Add(sheet);
                ws.Cells["A1"].LoadFromDataTable(tbl, true);
                MemoryStream ms = new MemoryStream();
                pack.SaveAs(ms);
                return ms;
                //ms.WriteTo(HttpContext.Current.Response.OutputStream);
                //Write it back to the client
                //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Response.AddHeader("content-disposition", string.Format("attachment;  filename={0}", "ExcellData.xlsx"));
                //Response.BinaryWrite(pck.GetAsByteArray());
            }
        }
        public DataSet readxls(string path)
        {

            FileStream stream = new FileStream(path, FileMode.Open);
            IExcelDataReader excelReader2003 = ExcelReaderFactory.CreateBinaryReader(stream);

            DataSet result = excelReader2003.AsDataSet();

            //foreach (DataTable table in result.Tables)
            //{
            //    for (int i = 0; i < table.Rows.Count; i++)
            //    {
            //        //for (int j = 0; j < table.Columns.Count; j++)
            //        //    Console.Write("\"" + table.Rows[i].ItemArray[j] + "\";");
            //        //Console.WriteLine();
            //    }
            //}

            excelReader2003.Close();
            return result;
        }

        public DataSet readxlsx(string path)
        {
            FileStream stream = new FileStream(path, FileMode.Open);
            IExcelDataReader excelReader2007 = ExcelReaderFactory.CreateOpenXmlReader(stream);
            DataSet result = excelReader2007.AsDataSet();

            //foreach (DataTable table in result.Tables)
            //{
            //    for (int i = 0; i < table.Rows.Count; i++)
            //    {
            //        //for (int j = 0; j < table.Columns.Count; j++)
            //        //    Console.Write("\"" + table.Rows[i].ItemArray[j] + "\";");
            //        //Console.WriteLine();
            //    }
            //}
            excelReader2007.Close();
            return result;
        }

        public void createxlsx()
        {

            //using (ExcelPackage pck = new ExcelPackage())
            //{
            //    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Logs");
            //    ws.Cells["A1"].LoadFromDataTable(dt, true);
            //    var ms = new System.IO.MemoryStream();
            //    pck.SaveAs(new FileInfo(@"c:\dev\New.xlsx"));
            //    pck.SaveAs(ms);
            //     //ms.WriteTo(Response.OutputStream);
            //    //Response.Clear();
            //    //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //    //Response.AddHeader("content-disposition", "attachment;filename=" + HttpUtility.UrlEncode("Logs.xlsx", System.Text.Encoding.UTF8));
            //}
            ExcelPackage ExcelPkg = new ExcelPackage();
            ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("Sheet1");
            using (ExcelRange Rng = wsSheet1.Cells[2, 2, 2, 2])
            {
                Rng.Value = "Welcome to Everyday be coding - tutorials for beginners";
                Rng.Style.Font.Size = 16;
                Rng.Style.Font.Bold = true;
                Rng.Style.Font.Italic = true;
            }
            wsSheet1.Protection.IsProtected = false;
            wsSheet1.Protection.AllowSelectLockedCells = false;
            ExcelPkg.SaveAs(new FileInfo(@"c:\dev\New.xlsx"));

        }
    }
}
